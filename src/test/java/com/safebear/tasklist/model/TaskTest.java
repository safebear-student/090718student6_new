package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import java.time.LocalDate;


public class TaskTest {
    @Test
    public void creation(){
        LocalDate localDate = LocalDate.now();
        Task task = new Task(1L, "Cooking", localDate, false);
        Assertions.assertThat(task.getId()).isEqualTo((1L));
// added for reference- (these would usually be individual unit tests) Different types of assertions
        Assertions.assertThat(task.getName()).isEqualTo("Cooking");
        Assertions.assertThat(task.getName()).isNotBlank();
        Assertions.assertThat(task.getDueDate()).isEqualTo(localDate);
        Assertions.assertThat(task.getCompleted()).isEqualTo(false);


    }
}
