package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

@DataJpaTest
@RunWith(SpringRunner.class)


public class TaskJpaTest {
    @Autowired
    private TestEntityManager tem;

    @Test
    public void mapping(){
        LocalDate localDate = LocalDate.now();
        Task task = this .tem.persistFlushFind(new Task(null, "Cooking", localDate, false));
        Assertions.assertThat(task.getId()).isNotNull();
    }


}
