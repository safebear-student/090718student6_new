Feature: Task management

  User story:
  In order to manage my tasks
  As a user
  I must be able to view, add, archive or update tasks

  Rules:
  - you must be able to view all tasks
  - You must be able to change the status
  - You must be able to see the name, due date and status of a task

  Questions:
  - How many tasks per page
  - Is there a reminder

  To do:
  - pagination

  Domain language:
  - task - description status and due date
  - Due date -  dd/mm/yyyy

# @to-do
# Background:
#  Given the following tasks are created
#  |cooking|
#  |gardening|

  @to-do
  Scenario: A user opens the homepage
    When the homepage opens
    Then the following tasks appear in the list
      |cooking|
      |gardening|

  Scenario Outline: A user creates a task
    When a user creates a <task>
    Then the <task> appears in the list
    Examples:
    |task|
    |cooking|
    |gardening|

